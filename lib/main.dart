import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:global_configuration/global_configuration.dart';

import 'app/app_module.dart';
import 'global_translations.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await GlobalConfiguration().loadFromPath("assets/config.json");
  await globalTranslations.init();
  return runApp(ModularApp(module: AppModule()));
}
