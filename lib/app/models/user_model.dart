class UserModel {
  int id;
  String name;
  String image;
  String email;
  String cep;
  String country;
  String state;
  String city;
  String district;
  String addressPerfil;
  String address;
  String number;
  String compl;
  String cpf;
  DateTime nascimento;
  String sexo;
  String estadoCivil;
  String celular;
  String phone;
  double renda;
  int politicamenteExposta;
  String codINSS;
  String digitoINSS;
  String profissao;
  int idCorretor;
  String corretor;
  int role;

  UserModel({
    this.id,
    this.name,
    this.image,
    this.email,
    this.cep,
    this.country,
    this.state,
    this.city,
    this.district,
    this.addressPerfil,
    this.cpf,
    this.nascimento,
    this.sexo,
    this.estadoCivil,
    this.profissao,
    this.number,
    this.compl,
    this.idCorretor,
    this.corretor,
    this.celular,
    this.role,
    this.address,
    this.phone,
    this.codINSS,
    this.digitoINSS,
    this.politicamenteExposta,
    this.renda
  });

  copyWith({
    int id,
    String name,
    String image,
    String email,
    String country,
    String state,
    String city,
    String district,
    String addressPerfil,
    String celular,
    String cpf,
    int role,
  }) {
    return UserModel(
      id: id ?? this.id,
      name: name ?? this.name,
      image: image ?? this.image,
      email: email ?? this.email,
      country: country ?? this.country,
      state: state ?? this.state,
      city: city ?? this.city,
      district: district ?? this.district,
      addressPerfil: addressPerfil ?? this.addressPerfil,
      celular: celular ?? this.celular,
      role: role ?? this.role,
      cpf: cpf ?? this.cpf,
    );
  }

  UserModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    name = json['name'];
    image = json['avatar'];
    email = json['email'];
    country = json['country'];
    state = json['state'];
    city = json['city'];
    district = json['district'];
    addressPerfil = json['address_perfil'];
    cpf = json['document'];
    sexo = json['sexo'];
    estadoCivil = json['estado_civil'];
    nascimento = json['birthday'] != null
        ? DateTime.parse(json['birthday'])
        : DateTime.now();
    celular = json['celular'];
    phone = json['phone'];
    renda = json['renda'] == 0 ? 0.0 : double.parse(json['renda']);
    politicamenteExposta =
        json['politicamente'] == null ? 0 : int.parse(json['politicamente']);
    codINSS = json["cod_inss"];
    digitoINSS = json["digito_inss"];
    cep = json["cep"];
    profissao = json["profissao"];
    number = json["number"];
    address = json["address"];
    compl = json["complemento"];
    idCorretor = int.parse(json["representante_id"]);
    corretor = json["representante"];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['image'] = this.image;
    return data;
  }
}
