class ConfigModel {
  int id;
  String key;
  String value;

  ConfigModel({
    this.id,
    this.key,
    this.value,
  });

  ConfigModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    key = json['key'];
    value = json['value'];
  }
}
