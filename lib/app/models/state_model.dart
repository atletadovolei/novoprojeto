class StateModel {
  int id;
  String state;
  String uf;

  StateModel({
    this.id,
    this.state,
    this.uf,
  });

  StateModel.fromJson(Map<String, dynamic> json) {
    id = int.parse(json['id']);
    state = json['name'];
    uf = json['uf'];
  }

  @override
  String toString() => state;    
}
