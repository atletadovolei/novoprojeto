import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'app_controller.dart';
import 'app_widget.dart';
import 'core/custom_dio/custom_dio.dart';
import 'modules/home/home_module.dart';
import 'repositories/app_repository.dart';
import 'splash_page.dart';
import 'stores/basic_store.dart';
import 'stores/config_store.dart';
import 'stores/user_store.dart';

class AppModule extends MainModule {
  @override
  List<Bind> get binds => [
        Bind((i) => AppController()),
        Bind((i) => AppRepository(i.get<CustomDio>())),
        Bind((i) => CustomDio(Dio())),
        Bind((i) => UserStore(i())),
        Bind((i) => ConfigStore(i())),
        Bind((i) => BasicStore(i())),
      ];

  @override
  List<ModularRouter> get routers => [
        ModularRouter(Modular.initialRoute, child: (_, args) => SplashPage()),
        ModularRouter("/home", module: HomeModule()),
        // ModularRouter("/login",
        //     module: LoginModule(),
        //     transition: TransitionType.leftToRightWithFade),
        // ModularRouter("/register",
        //     module: RegisterModule(),
        //     transition: TransitionType.rightToLeftWithFade),
      ];

  @override
  Widget get bootstrap => AppWidget();

  static Inject get to => Inject<AppModule>.of();
}
