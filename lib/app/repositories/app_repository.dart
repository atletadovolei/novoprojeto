import 'dart:convert';
import 'package:dio/dio.dart';
import '../models/config_model.dart';
import '../models/state_model.dart';
import '../models/user_model.dart';
import '../core/local_storage.dart';
import '../core/custom_dio/custom_dio.dart';

class AppRepository {
  final CustomDio dio;

  AppRepository(this.dio);

  Future<String> loginEmailPassowrd(String usuario, String senha) async {
    try {
      String retorno;

      Map params = {
        'username': usuario,
        'password': senha,
        'type': 'login',
        'field': 'document'
      };

      var response = await dio.client.post("/users", data: params);
      if (response.data["status"] == true) {
        retorno = "";
        await LocalStorage.setValue("user", json.encode(response.data));
      } else {
        retorno = response.data["msg"];
      }

      return retorno;
    } on DioError catch (e) {
      throw (e.message);
    }
  }

  Future<List<ConfigModel>> getConfig() async {
    try {
      var response = await dio.client.get("/users/config");

      return (response.data as List)
          .map((item) => ConfigModel.fromJson(item))
          .toList();
    } on DioError catch (e) {
      throw (e.message);
    }
  }

  Future<List<StateModel>> getStates() async {
    try {
      var response = await dio.client.get("/address/states");

      return (response.data as List)
          .map((item) => StateModel.fromJson(item))
          .toList();
    } on DioError catch (e) {
      throw (e.message);
    }
  }  

  Future<Map<String, dynamic>> saveHome(
      String sessao, Map<String, dynamic> data) async {
    try {
      var response = await dio.client.post("/" + sessao, data: data);
      return response.data;
    } on DioError catch (e) {
      throw (e.message);
    }
  }

  Future<UserModel> updatePessoa(Map<String, dynamic> data) async {
    var response = await dio.client.post("/users", data: data);
    return UserModel.fromJson(response.data);
  }  
}
