import 'package:flutter/material.dart';
import 'package:flutter_modular/flutter_modular.dart';

import 'core/costants.dart';
import 'stores/config_store.dart';

class SplashPage extends StatefulWidget {
  SplashPage({Key key}) : super(key: key);

  @override
  _SplashPageState createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage> {
  List<Future> precaches = [
    Future.delayed(Duration(seconds: 3)),
    Modular.get<ConfigStore>().getConfig()
  ];

  @override
  void initState() {
    super.initState();
    Future.wait(precaches).then(
      (value) async {
        // final auth = Modular.get<AuthController>();

        // bool info = await LocalStorage.getBool("loginAutomatico");
        // await Future.delayed(Duration(milliseconds: 500));
        // if (info == false || info == null) {
        //   await LocalStorage.setValue("userEmail", "");
        //   await LocalStorage.setBool("loginAutomatico", true);
        //   Modular.to.pushReplacementNamed('/login');
        // } else {
        //   String uuser = await LocalStorage.getValue("userEmail");
        //   String usena = await LocalStorage.getValue("userSenha");
        //   await auth.login(uuser, usena);
        //   Modular.to.pushReplacementNamed('/home');
        // }

        await Future.delayed(Duration(seconds: 3));
        Modular.to.pushReplacementNamed('/home');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: corWhite,
      body: Container(
        child: Center(
          child: Image.asset(
            "assets/img/app-icon.png",
            width: MediaQuery.of(context).size.width * .5,
          ),
        ),
      ),
    );
  }
}
