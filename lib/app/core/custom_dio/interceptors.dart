import 'package:dio/dio.dart';

class CustomInterceptors extends InterceptorsWrapper {
  @override
  onRequest(RequestOptions options) async {
    options.contentType = "application/x-www-form-urlencoded";
    print(
        'REQUEST[${options.method}] => PATH: ${options.path} - Data: ${options.data} - Content Type: ${options.headers}');
    return options;
  }

  @override
  onResponse(Response response) async {
    //200
    //201
    print('RESPONSE[${response.statusCode}] => PATH: ${response.request.path}');
    return response;
  }

  @override
  onError(DioError e) async {
    //Exception
    print('ERROR[${e.response.statusCode}] => PATH: ${e.request.path}');
    return e;
  }
}
