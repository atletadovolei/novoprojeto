import 'package:dio/dio.dart';
import 'package:global_configuration/global_configuration.dart';
import 'interceptors.dart';

class CustomDio {
  final Dio client;

  CustomDio(this.client) {
    client.options.baseUrl = GlobalConfiguration().getValue('BASE_URL');
    client.options.connectTimeout = 5000;
    client.interceptors.add(CustomInterceptors());
  }
}
