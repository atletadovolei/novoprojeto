import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GlobalScaffold {
  static final GlobalScaffold instance = GlobalScaffold();
  final scaffKey = GlobalKey<ScaffoldState>();
  void showSnackBar(SnackBar snackbar) {
    scaffKey.currentState.showSnackBar(snackbar);
  }
}