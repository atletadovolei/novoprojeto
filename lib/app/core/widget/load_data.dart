import 'package:flutter/material.dart';

import '../helpers.dart';

class LoadData extends StatelessWidget {
  final double width;
  final double height;
  const LoadData({Key key, this.width = 50, this.height = 50}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width,
      height: height,
      child: Center(
        child: showImageAssets("load.gif"),
      ),
    );
  }
}
