import 'package:Modelo/app/core/costants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class RSAInputText extends StatelessWidget {
  final String texto;
  final String dica;
  final bool passw;
  final bool autofocus;
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final TextInputType keyboardType;
  final TextInputAction textInputAction;
  final FocusNode focusNode;
  final FocusNode nextFocus;
  final int maxLength;
  final Function onChange;
  final String newError;
  final GestureTapCallback onTap;
  final IconData icone;
  final IconData iconePrefix;
  final IconData iconeSufix;
  final double sizeFont;
  final double sizeHint;
  final Function tapSufix;
  final List<TextInputFormatter> formatter;
  final double paddingLeftInt;
  final double paddingRightInt;
  final double paddingTopInt;
  final double paddingBottomInt;
  final bool isDense;
  final Color iconeColor;
  final Color iconePrefixColor;
  final Color iconeSufixColor;
  final bool border;
  final Color borderColorCheck;
  final Color borderColorInactive;
  final Color colorBackground;
  final double borderRadius;
  final double paddingLeft;
  final double paddingRight;
  final double paddingBottom;
  final double paddingTop;

  RSAInputText(
    this.texto, {
    this.maxLength = 0,
    this.onChange,
    this.tapSufix,
    this.dica,
    this.passw = false,
    this.autofocus = false,
    this.controller,
    this.validator,
    this.keyboardType,
    this.textInputAction,
    this.focusNode,
    this.nextFocus,
    this.newError,
    this.onTap,
    this.icone,
    this.iconePrefix,
    this.sizeFont = 13,
    this.sizeHint = 13,
    this.iconeSufix,
    this.formatter,
    this.paddingLeftInt = 10.0,
    this.paddingRightInt = 10.0,
    this.paddingBottomInt = 3.0,
    this.paddingTopInt = 3.0,
    this.isDense = false,
    this.iconeColor = corPrincipal,
    this.iconePrefixColor = corPrincipal,
    this.iconeSufixColor = corPrincipal,
    this.border = true,
    this.colorBackground = null,
    this.borderColorCheck = corPrincipal,
    this.borderColorInactive = corPrincipal,
    this.borderRadius = 15,
    this.paddingLeft = 10.0,
    this.paddingRight = 10.0,
    this.paddingBottom = 3.0,
    this.paddingTop = 3.0,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(paddingLeft, paddingTop, paddingRight, paddingBottom),
      child: Theme(
        data: Theme.of(context).copyWith(
            primaryColor: borderColorCheck, unselectedWidgetColor: corRed),
        child: TextFormField(
          onChanged: onChange,
          onTap: onTap,
          obscureText: passw,
          controller: controller,
          validator: validator,
          keyboardType: keyboardType,
          textInputAction: textInputAction,
          focusNode: focusNode,
          autofocus: autofocus,
          onFieldSubmitted: (String text) {
            if (nextFocus != null) {
              FocusScope.of(context).requestFocus(nextFocus);
            }
          },
          style: TextStyle(fontSize: 13),
          maxLength: maxLength > 0 ? maxLength : null,
          inputFormatters: formatter == null ? null : formatter,
          decoration: InputDecoration(
            filled: colorBackground == null ? false : true,
            fillColor: colorBackground,
            icon: icone == null
                ? null
                : Icon(
                    icone,
                    size: sizeFont,
                    color: iconeColor,
                  ),
            prefixIcon: iconePrefix == null
                ? null
                : Icon(
                    iconePrefix,
                    size: sizeFont + 5,
                    color: iconePrefixColor,
                  ),
            suffixIcon: tapSufix == null
                ? null
                : GestureDetector(
                    onTap: tapSufix,
                    child: iconeSufix == null
                        ? null
                        : Icon(
                            iconeSufix,
                            size: sizeFont + 5,
                            color: iconeSufixColor,
                          ),
                  ),
            isDense: isDense,
            contentPadding: EdgeInsets.fromLTRB(
              paddingLeftInt,
              paddingTopInt,
              paddingRightInt,
              paddingBottomInt,
            ),
            counterText: "",
            border: border
                ? OutlineInputBorder(
                    borderRadius: BorderRadius.circular(borderRadius),
                  )
                : null,
            labelText: texto,
            labelStyle: TextStyle(fontSize: 13),
            hintText: dica,
            errorText: newError,
          ),
        ),
      ),
    );
  }
}
