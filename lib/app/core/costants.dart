import 'package:flutter/material.dart';

const Color corPrincipal = Color(0xff4CB86A);
const Color corSecundaria = Color(0xffFD7421);
const Color darkBlue = Color(0xff071d40);
const Color lightBlue = Color(0xff1b4dff);
const Color corBotao = Color(0xff81BEF7);
const Color corWhite = Colors.white;
const Color corInativa = Color(0xff999999);
const Color corRed = Color(0xffFC582F);
const Color corGreen = Colors.green;
const Color corGrey = Colors.grey;
const Color corTransparente = Colors.transparent;
