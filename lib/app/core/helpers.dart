import 'dart:convert';
import 'dart:math';
import 'package:Modelo/app/stores/basic_store.dart';
import 'package:cloudinary_client/cloudinary_client.dart';
import 'package:cloudinary_client/models/CloudinaryResponse.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:flutter_modular/flutter_modular.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:oktoast/oktoast.dart';
import 'package:path_provider/path_provider.dart' as path_provider;
import 'costants.dart';
import 'global_scaffold.dart';
import 'widget/load_data.dart';

CachedNetworkImageProvider showImageProvider(String image) {
  return CachedNetworkImageProvider(image);
}

CachedNetworkImage showImage(
  String image, {
  double height = 40.0,
  double width = double.infinity,
  BoxFit boxFit = BoxFit.cover,
  double borderRadius = 0,
}) {
  return CachedNetworkImage(
    imageUrl: image,
    imageBuilder: (context, imageProvider) => Container(
      height: height,
      width: width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(borderRadius),
        image: DecorationImage(
          image: imageProvider,
          fit: boxFit,
        ),
      ),
    ),
    placeholder: (context, url) => Center(
      child: LoadData(),
    ), //
    errorWidget: (context, url, error) => Icon(Icons.error),
  );
}

Widget showImageAssets(String image,
    {double height = 40.0,
    double width = double.infinity,
    BoxFit boxFit = BoxFit.cover,
    double borderRadius = 0,
    bool interno = false}) {
  return Container(
    height: height,
    width: width,
    decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(borderRadius),
      image: DecorationImage(
        image: interno ? AssetImage(image) : AssetImage("assets/img/" + image),
        fit: boxFit,
      ),
    ),
  );
}

messageSys(
  String message, {
  Color colorBackground = Colors.white,
  Color corTexto = Colors.black,
  double fontSize = 13,
  double padding = 16,
  int tempoShow = 5,
  ToastPosition position = ToastPosition.center,
  bool dispensarMultiplosToast = true,
  bool retorno = true,
}) {
  // retorno Quer dizer que quando a funnção retornar FALSE vai emitir message padrão
  if (retorno == false) {
    message = "problem";
  }

  switch (message) {
    case "nointernet":
      message = "Verifique sua conexão com a internet!";
      colorBackground = Colors.red;
      corTexto = Colors.white;
      break;
    case "problem":
      message = "Houve algum erro! Tente de novo.";
      colorBackground = Colors.red;
      corTexto = Colors.white;
      position = ToastPosition.bottom;
      break;

    case "noready":
      message =
          "Ops. Este recuros ainda não está disponível. Estamos trabalhando para disponibilizar o mesmo o quanto antes, agradecemos sua compreensão! Ajude o Atleta do Vôlei, contate-nos.";
      colorBackground = Colors.red;
      corTexto = Colors.white;
      fontSize = 14;
      break;
    default:
      colorBackground = Colors.transparent;
      corTexto = Colors.black;
  }
  showToast(
    message,
    backgroundColor: colorBackground,
    textPadding: EdgeInsets.all(padding),
    textStyle: TextStyle(color: corTexto, fontSize: fontSize),
    duration: Duration(seconds: tempoShow),
    position: ToastPosition.center,
    dismissOtherToast: dispensarMultiplosToast,
    animationCurve: Curves.easeInExpo,
    animationBuilder: Miui10AnimBuilder(),
    animationDuration: Duration(milliseconds: 200),
  );
}

getSetting(String key) {
  BasicStore basic = Modular.get();

  String info = basic.config.firstWhere((element) => element.key == key).value;
  return info == null ? "" : info;
}

getSettingSub(String key, String retorno) {
  BasicStore basic = Modular.get();

  String data = basic.config.firstWhere((element) => element.key == key).value;
  Map<String, dynamic> lista = jsonDecode(data);
  return lista[retorno] == null ? "" : lista[retorno];
}

String formatMoeda(double value, {bool cifra = true}) {
  if (value == null) {
    return cifra ? "R\$ 0,00" : "0,00";
  } else {
    return cifra
        ? NumberFormat.currency(locale: "pt_BR", name: "R\$").format(
            value,
          )
        : NumberFormat.compact().format(
            value,
          );
  }
}

String formatData(DateTime value, {bool showTime = false}) {
  if (showTime) {
    return DateFormat("dd/MM/yyyy hh:mm", "pt_BR").format(value);
  } else {
    return DateFormat("dd/MM/yyyy", "pt_BR").format(value);
  }
}

messageSnackBar(String message,
    {Color color = corRed, IconData icon = Icons.cancel, int time = 3}) {
  return GlobalScaffold.instance.showSnackBar(
    SnackBar(
      duration: Duration(seconds: time),
      backgroundColor: color,
      content: Row(
        children: <Widget>[
          Icon(icon),
          SizedBox(width: 5),
          Flexible(child: Text(message)),
        ],
      ),
    ),
  );
}

uploadCloudinary(String pathImage) async {
  try {
    CloudinaryClient client = CloudinaryClient(
      getSettingSub("cloudinary", "api_key"),
      getSettingSub("cloudinary", "api_secret"),
      getSettingSub("cloudinary", "cloudname"),
    );
    CloudinaryResponse resp = await client.uploadImage(pathImage);
    return {
      "success": true,
      "msg": "",
      "link": resp.secure_url,
    };
  } catch (e) {
    if (e.response.statusCode == 400) {
      return {
        "success": false,
        "msg": "API Cloudinary não configurada!",
        "link": "",
      };
    }
  }
}

Future takePhoto({String tipo = "camera"}) async {
  ImageSource source;
  final Random _random = Random();
  final picker = ImagePicker();

  if (tipo == "camera") {
    source = ImageSource.camera;
  } else {
    source = ImageSource.gallery;
  }

  final dir = await path_provider.getTemporaryDirectory();
  final targetPath = dir.absolute.path +
      "/" +
      _random.nextInt(99999999).toString() +
      "_temp.jpg";

  final banner = await picker.getImage(source: source);

  if (banner != null) {
    final cropped = await ImageCropper.cropImage(
      sourcePath: banner.path,
      aspectRatioPresets: [
        CropAspectRatioPreset.square,
        CropAspectRatioPreset.ratio3x2,
        CropAspectRatioPreset.original,
        CropAspectRatioPreset.ratio4x3,
        CropAspectRatioPreset.ratio16x9
      ],
      androidUiSettings: AndroidUiSettings(
        toolbarTitle: 'Redimensione',
        toolbarColor: corPrincipal,
        toolbarWidgetColor: corWhite,
        initAspectRatio: CropAspectRatioPreset.ratio4x3,
        lockAspectRatio: false,
      ),
      iosUiSettings: IOSUiSettings(
        minimumAspectRatio: 1.0,
      ),
    );

    var compressed = await FlutterImageCompress.compressAndGetFile(
      cropped.path,
      targetPath,
      quality: 80,
      rotate: 0,
    );

    String pathFinal = compressed.path;

    return pathFinal;
  } else {
    return "";
  }
}
