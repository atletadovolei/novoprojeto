import 'dart:convert';
import 'package:Modelo/app/core/helpers.dart';
import 'package:Modelo/app/models/user_model.dart';
import 'package:mobx/mobx.dart';

import '../core/local_storage.dart';
import '../repositories/app_repository.dart';

part 'user_store.g.dart';

class UserStore = _UserStoreBase with _$UserStore;

enum AuthStatus { loading, login, logoff }

abstract class _UserStoreBase with Store {
  final AppRepository repository;

  _UserStoreBase(this.repository);

  @observable
  bool loading = false;

  @action
  changeLoading(bool value) => loading = value;

  @action
  logOut() async {
    logado = false;
    statusLogin = AuthStatus.logoff;
    changeStatusStr("logoff");

    await LocalStorage.setValue("userEmail", "");
    await LocalStorage.setValue("userSenha", "");
    await LocalStorage.setBool("statusLogin", false);
  }

  @action
  Future<String> login(String edtUser, String edtSenha) async {
    String status = "";

    if (statusLogin != AuthStatus.login) {
      status = await repository.loginEmailPassowrd(edtUser, edtSenha);
      if (status == "") {
        await LocalStorage.setValue("userEmail", edtUser);
        await LocalStorage.setValue("userSenha", edtSenha);
        changeStatusStr("login");
      } else {
        changeStatusStr("logoff");
      }

      await getStatusLogin();
    }
    return status;
  }

  @observable
  bool logado = false;

  @action
  changeStatusLogado(bool value) => logado = value;

  @observable
  String statusLoginStr;

  @observable
  AuthStatus statusLogin = AuthStatus.logoff;

  @action
  changeStatusStr(String value) => statusLoginStr = value;

  @action
  getStatusLogin() async {
    switch (statusLoginStr) {
      case "loading":
        await LocalStorage.setValue("statusLogin", "loading");
        statusLogin = AuthStatus.loading;
        changeStatusLogado(false);
        break;
      case "login":
        await LocalStorage.setValue("statusLogin", "login");
        changeStatusLogado(true);
        statusLogin = AuthStatus.login;
        getCliente();
        break;
      case "logoff":
        await LocalStorage.setValue("statusLogin", "logoff");
        changeStatusLogado(false);
        statusLogin = AuthStatus.logoff;
        break;
      default:
        await LocalStorage.setValue("statusLogin", "loading");
        changeStatusLogado(false);
        statusLogin = AuthStatus.loading;
    }
  }

  @observable
  UserModel user;

  @observable
  String avatarUser;

  @action
  getCliente() async {
    if (await LocalStorage.getValue("statusLogin") == "login") {
      final dados = json.decode(await LocalStorage.getValue("user"));
      user = UserModel.fromJson(dados);
      avatarUser = user.image;
    }
  }

  @action
  Future<bool> saveConfig(String field, String value, String atleta) async {
    try {
      Map<String, dynamic> data = {
        "id_user": atleta,
        "field": field,
        "value": value,
        "type": "update_config"
      };

      Map<String, dynamic> retorno = await repository.saveHome("config", data);

      if (retorno["success"]) {
        return true;
      } else {
        return false;
      }
    } catch (e) {
      print("Erro: " + e);
      return false;
    }
  }

  @action
  Future<bool> updateAvatar(String avatar) async {
    try {
      Map<String, dynamic> strAvatar = await uploadCloudinary(avatar);

      if (strAvatar["success"]) {
        Map<String, dynamic> data = {
          "id_user": user.id,
          "avatar": strAvatar["link"],
          "type": "update_avatar"
        };

        Map<String, dynamic> retorno = await repository.saveHome("users", data);

        if (retorno["success"]) {
          avatarUser = strAvatar["link"];
          return true;
        } else {
          message = retorno["msg"];
          return false;
        }
      } else {
        message = strAvatar["msg"];
        return false;
      }
    } catch (e) {
      print("Erro: " + e);
      return false;
    }
  }

  @observable
  String message = "";

  @action
  Future<bool> register(Map<String, dynamic> data) async {
    try {
      Map<String, dynamic> retorno = await repository.saveHome("users", data);
      if (retorno["success"]) {
        message = "";
        return true;
      } else {
        message = retorno["msg"];
        return false;
      }
    } catch (e) {
      print("Erro: " + e);
      return false;
    }
  }

  @observable
  int recoveryToken;

  @observable
  String recoveryEmail = "";

  @observable
  String recoveryTelefone = "";

  @observable
  String recoverySenha;

  @observable
  int recoveryUser;

  @action
  changeString(String value, String variable) {
    if (variable == "recoverySenha") recoverySenha = value.trim();
  }

  @action
  Future<bool> recovery(String field, String valor,
      {String tipo = "sms"}) async {
    try {
      if (valor == null || valor == "") {
        message = "Informe o CPF para continuar...";
        return false;
      }

      Map<String, dynamic> data = {
        "field": field,
        "value": valor,
        "formato": tipo,
        "type": "recovery"
      };

      Map<String, dynamic> retorno = await repository.saveHome("users", data);
      if (retorno["success"]) {
        message = "";
        recoveryToken = int.parse(retorno["token_gerado"].toString());
        recoveryUser = int.parse(retorno["id_user"].toString());
        recoveryEmail =
            retorno["token_email"] == null ? "" : retorno["token_email"];
        recoveryTelefone =
            retorno["token_celular"] == null ? "" : retorno["token_celular"];
        return true;
      } else {
        if (retorno["token"]) {
          message = "";
          recoveryToken = int.parse(retorno["token_gerado"].toString());
          recoveryUser = int.parse(retorno["id_user"].toString());
          recoveryEmail =
              retorno["token_email"] == null ? "" : retorno["token_email"];
          recoveryTelefone =
              retorno["token_celular"] == null ? "" : retorno["token_celular"];
          return true;
        } else {
          message = retorno["msg"];
        }
        return false;
      }
    } catch (e) {
      print("Erro: " + e);
      return false;
    }
  }

  @action
  Future<bool> updateSenha() async {
    if (recoverySenha == null || recoverySenha == "") {
      message = "Informe uma nova senha para continuar!";
      return false;
    }
    try {
      Map<String, dynamic> data = {
        "newpass": recoverySenha,
        "user": recoveryUser,
        "codeToken": recoveryToken,
        "type": "confirm_recovery"
      };
      Map<String, dynamic> retorno = await repository.saveHome("users", data);
      if (retorno["success"]) {
        message = "";
        return true;
      } else {
        message = retorno["msg"];
        return false;
      }
    } catch (e) {
      print("Erro: " + e);
      return false;
    }
  }

  @action
  Future<bool> updatePassword(String passw) async {
    
    try {
      Map<String, dynamic> data = {
        "newpass": passw,
        "user": user.id,
        "type": "update_passw"
      };
      Map<String, dynamic> retorno = await repository.saveHome("users", data);
      if (retorno["success"]) {
        message = "";
        return true;
      } else {
        message = retorno["msg"];
        return false;
      }
    } catch (e) {
      print("Erro: " + e);
      return false;
    }
  }  

  @action
  Future<bool> updateProfile(Map<String, dynamic> dados) async {
    try {
      Map<String, dynamic> data = {
        "nome": dados["nome"],
        "email": dados["email"],
        "telefone": dados["telefone"],
        "document": dados["document"],
        "id_user": user.id,
        "type": "update_profile"
      };
      Map<String, dynamic> retorno = await repository.saveHome("users", data);
      if (retorno["success"]) {
        user = user.copyWith(
          name: dados["nome"],
          email: dados["email"],
          celular: dados["telefone"],
          cpf: dados["document"],
        );

        message = "";
        return true;
      } else {
        message = retorno["msg"];
        return false;
      }
    } catch (e) {
      print("Erro: " + e);
      return false;
    }
  }

  @observable
  String tokenFirebase;
}