// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$UserStore on _UserStoreBase, Store {
  final _$loadingAtom = Atom(name: '_UserStoreBase.loading');

  @override
  bool get loading {
    _$loadingAtom.reportRead();
    return super.loading;
  }

  @override
  set loading(bool value) {
    _$loadingAtom.reportWrite(value, super.loading, () {
      super.loading = value;
    });
  }

  final _$logadoAtom = Atom(name: '_UserStoreBase.logado');

  @override
  bool get logado {
    _$logadoAtom.reportRead();
    return super.logado;
  }

  @override
  set logado(bool value) {
    _$logadoAtom.reportWrite(value, super.logado, () {
      super.logado = value;
    });
  }

  final _$statusLoginStrAtom = Atom(name: '_UserStoreBase.statusLoginStr');

  @override
  String get statusLoginStr {
    _$statusLoginStrAtom.reportRead();
    return super.statusLoginStr;
  }

  @override
  set statusLoginStr(String value) {
    _$statusLoginStrAtom.reportWrite(value, super.statusLoginStr, () {
      super.statusLoginStr = value;
    });
  }

  final _$statusLoginAtom = Atom(name: '_UserStoreBase.statusLogin');

  @override
  AuthStatus get statusLogin {
    _$statusLoginAtom.reportRead();
    return super.statusLogin;
  }

  @override
  set statusLogin(AuthStatus value) {
    _$statusLoginAtom.reportWrite(value, super.statusLogin, () {
      super.statusLogin = value;
    });
  }

  final _$userAtom = Atom(name: '_UserStoreBase.user');

  @override
  UserModel get user {
    _$userAtom.reportRead();
    return super.user;
  }

  @override
  set user(UserModel value) {
    _$userAtom.reportWrite(value, super.user, () {
      super.user = value;
    });
  }

  final _$avatarUserAtom = Atom(name: '_UserStoreBase.avatarUser');

  @override
  String get avatarUser {
    _$avatarUserAtom.reportRead();
    return super.avatarUser;
  }

  @override
  set avatarUser(String value) {
    _$avatarUserAtom.reportWrite(value, super.avatarUser, () {
      super.avatarUser = value;
    });
  }

  final _$messageAtom = Atom(name: '_UserStoreBase.message');

  @override
  String get message {
    _$messageAtom.reportRead();
    return super.message;
  }

  @override
  set message(String value) {
    _$messageAtom.reportWrite(value, super.message, () {
      super.message = value;
    });
  }

  final _$recoveryTokenAtom = Atom(name: '_UserStoreBase.recoveryToken');

  @override
  int get recoveryToken {
    _$recoveryTokenAtom.reportRead();
    return super.recoveryToken;
  }

  @override
  set recoveryToken(int value) {
    _$recoveryTokenAtom.reportWrite(value, super.recoveryToken, () {
      super.recoveryToken = value;
    });
  }

  final _$recoveryEmailAtom = Atom(name: '_UserStoreBase.recoveryEmail');

  @override
  String get recoveryEmail {
    _$recoveryEmailAtom.reportRead();
    return super.recoveryEmail;
  }

  @override
  set recoveryEmail(String value) {
    _$recoveryEmailAtom.reportWrite(value, super.recoveryEmail, () {
      super.recoveryEmail = value;
    });
  }

  final _$recoveryTelefoneAtom = Atom(name: '_UserStoreBase.recoveryTelefone');

  @override
  String get recoveryTelefone {
    _$recoveryTelefoneAtom.reportRead();
    return super.recoveryTelefone;
  }

  @override
  set recoveryTelefone(String value) {
    _$recoveryTelefoneAtom.reportWrite(value, super.recoveryTelefone, () {
      super.recoveryTelefone = value;
    });
  }

  final _$recoverySenhaAtom = Atom(name: '_UserStoreBase.recoverySenha');

  @override
  String get recoverySenha {
    _$recoverySenhaAtom.reportRead();
    return super.recoverySenha;
  }

  @override
  set recoverySenha(String value) {
    _$recoverySenhaAtom.reportWrite(value, super.recoverySenha, () {
      super.recoverySenha = value;
    });
  }

  final _$recoveryUserAtom = Atom(name: '_UserStoreBase.recoveryUser');

  @override
  int get recoveryUser {
    _$recoveryUserAtom.reportRead();
    return super.recoveryUser;
  }

  @override
  set recoveryUser(int value) {
    _$recoveryUserAtom.reportWrite(value, super.recoveryUser, () {
      super.recoveryUser = value;
    });
  }

  final _$tokenFirebaseAtom = Atom(name: '_UserStoreBase.tokenFirebase');

  @override
  String get tokenFirebase {
    _$tokenFirebaseAtom.reportRead();
    return super.tokenFirebase;
  }

  @override
  set tokenFirebase(String value) {
    _$tokenFirebaseAtom.reportWrite(value, super.tokenFirebase, () {
      super.tokenFirebase = value;
    });
  }

  final _$logOutAsyncAction = AsyncAction('_UserStoreBase.logOut');

  @override
  Future logOut() {
    return _$logOutAsyncAction.run(() => super.logOut());
  }

  final _$loginAsyncAction = AsyncAction('_UserStoreBase.login');

  @override
  Future<String> login(String edtUser, String edtSenha) {
    return _$loginAsyncAction.run(() => super.login(edtUser, edtSenha));
  }

  final _$getStatusLoginAsyncAction =
      AsyncAction('_UserStoreBase.getStatusLogin');

  @override
  Future getStatusLogin() {
    return _$getStatusLoginAsyncAction.run(() => super.getStatusLogin());
  }

  final _$getClienteAsyncAction = AsyncAction('_UserStoreBase.getCliente');

  @override
  Future getCliente() {
    return _$getClienteAsyncAction.run(() => super.getCliente());
  }

  final _$saveConfigAsyncAction = AsyncAction('_UserStoreBase.saveConfig');

  @override
  Future<bool> saveConfig(String field, String value, String atleta) {
    return _$saveConfigAsyncAction
        .run(() => super.saveConfig(field, value, atleta));
  }

  final _$updateAvatarAsyncAction = AsyncAction('_UserStoreBase.updateAvatar');

  @override
  Future<bool> updateAvatar(String avatar) {
    return _$updateAvatarAsyncAction.run(() => super.updateAvatar(avatar));
  }

  final _$registerAsyncAction = AsyncAction('_UserStoreBase.register');

  @override
  Future<bool> register(Map<String, dynamic> data) {
    return _$registerAsyncAction.run(() => super.register(data));
  }

  final _$recoveryAsyncAction = AsyncAction('_UserStoreBase.recovery');

  @override
  Future<bool> recovery(String field, String valor, {String tipo = "sms"}) {
    return _$recoveryAsyncAction
        .run(() => super.recovery(field, valor, tipo: tipo));
  }

  final _$updateSenhaAsyncAction = AsyncAction('_UserStoreBase.updateSenha');

  @override
  Future<bool> updateSenha() {
    return _$updateSenhaAsyncAction.run(() => super.updateSenha());
  }

  final _$updatePasswordAsyncAction =
      AsyncAction('_UserStoreBase.updatePassword');

  @override
  Future<bool> updatePassword(String passw) {
    return _$updatePasswordAsyncAction.run(() => super.updatePassword(passw));
  }

  final _$updateProfileAsyncAction =
      AsyncAction('_UserStoreBase.updateProfile');

  @override
  Future<bool> updateProfile(Map<String, dynamic> dados) {
    return _$updateProfileAsyncAction.run(() => super.updateProfile(dados));
  }

  final _$_UserStoreBaseActionController =
      ActionController(name: '_UserStoreBase');

  @override
  dynamic changeLoading(bool value) {
    final _$actionInfo = _$_UserStoreBaseActionController.startAction(
        name: '_UserStoreBase.changeLoading');
    try {
      return super.changeLoading(value);
    } finally {
      _$_UserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeStatusLogado(bool value) {
    final _$actionInfo = _$_UserStoreBaseActionController.startAction(
        name: '_UserStoreBase.changeStatusLogado');
    try {
      return super.changeStatusLogado(value);
    } finally {
      _$_UserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeStatusStr(String value) {
    final _$actionInfo = _$_UserStoreBaseActionController.startAction(
        name: '_UserStoreBase.changeStatusStr');
    try {
      return super.changeStatusStr(value);
    } finally {
      _$_UserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic changeString(String value, String variable) {
    final _$actionInfo = _$_UserStoreBaseActionController.startAction(
        name: '_UserStoreBase.changeString');
    try {
      return super.changeString(value, variable);
    } finally {
      _$_UserStoreBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
loading: ${loading},
logado: ${logado},
statusLoginStr: ${statusLoginStr},
statusLogin: ${statusLogin},
user: ${user},
avatarUser: ${avatarUser},
message: ${message},
recoveryToken: ${recoveryToken},
recoveryEmail: ${recoveryEmail},
recoveryTelefone: ${recoveryTelefone},
recoverySenha: ${recoverySenha},
recoveryUser: ${recoveryUser},
tokenFirebase: ${tokenFirebase}
    ''';
  }
}
