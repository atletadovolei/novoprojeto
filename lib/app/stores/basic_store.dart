import 'package:Modelo/app/models/config_model.dart';
import 'package:mobx/mobx.dart';
import '../repositories/app_repository.dart';

part 'basic_store.g.dart';

class BasicStore = _BasicStoreBase with _$BasicStore;

abstract class _BasicStoreBase with Store {
  final AppRepository repository;

  _BasicStoreBase(this.repository);


  @observable
  bool notification = false;

  @observable
  ObservableList<ConfigModel> config;
}