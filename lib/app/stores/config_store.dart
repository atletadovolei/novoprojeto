import '../models/config_model.dart';
import '../repositories/app_repository.dart';
import 'package:mobx/mobx.dart';
part 'config_store.g.dart';

class ConfigStore = _ConfigStoreBase with _$ConfigStore;

abstract class _ConfigStoreBase with Store {
  final AppRepository repository;

  @observable
  ObservableList<ConfigModel> config;

  _ConfigStoreBase(this.repository);

  @action
  getConfig() async {
    config = (await repository.getConfig()).asObservable();
  }
}
