// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'basic_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BasicStore on _BasicStoreBase, Store {
  final _$notificationAtom = Atom(name: '_BasicStoreBase.notification');

  @override
  bool get notification {
    _$notificationAtom.reportRead();
    return super.notification;
  }

  @override
  set notification(bool value) {
    _$notificationAtom.reportWrite(value, super.notification, () {
      super.notification = value;
    });
  }

  final _$configAtom = Atom(name: '_BasicStoreBase.config');

  @override
  ObservableList<ConfigModel> get config {
    _$configAtom.reportRead();
    return super.config;
  }

  @override
  set config(ObservableList<ConfigModel> value) {
    _$configAtom.reportWrite(value, super.config, () {
      super.config = value;
    });
  }

  @override
  String toString() {
    return '''
notification: ${notification},
config: ${config}
    ''';
  }
}
