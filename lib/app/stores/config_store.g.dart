// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'config_store.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ConfigStore on _ConfigStoreBase, Store {
  final _$configAtom = Atom(name: '_ConfigStoreBase.config');

  @override
  ObservableList<ConfigModel> get config {
    _$configAtom.reportRead();
    return super.config;
  }

  @override
  set config(ObservableList<ConfigModel> value) {
    _$configAtom.reportWrite(value, super.config, () {
      super.config = value;
    });
  }

  final _$getConfigAsyncAction = AsyncAction('_ConfigStoreBase.getConfig');

  @override
  Future getConfig() {
    return _$getConfigAsyncAction.run(() => super.getConfig());
  }

  @override
  String toString() {
    return '''
config: ${config}
    ''';
  }
}
